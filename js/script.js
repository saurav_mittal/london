// Document Object Model
window.onload = function(){
var backgroundImage= document.getElementById("header");
var background= [
	{background:"images/image4.jpg"},
    {background:"images/image5.jpg"},
    {background:"images/image6.jpg"},
    {background:"images/image8.jpg"},
    {background:"images/image9.jpg"},
    ];
var counter=0;
var sliderbuttons = document.getElementById("slider_nav");
var sliderInterval;
var startSlider = document.getElementById("startslider");
var stopslider= document.getElementById("stopslider");
var nextbutton=document.getElementById("nextbutton");
var prev=document.getElementById("prevbutton");
var headerElement = document.getElementById("header");
displayButtons = function(){
	sliderbuttons.style.display = "block"; 
}
hideButtons = function(){
	sliderbuttons.style.display = "none"; 
}
next=function(){
try{
counter++;
backgroundImage.style.backgroundImage = "url('"+background[counter].background+"')";
}
catch(err){
	counter=0;
	backgroundImage.style.backgroundImage = "url('"+background[counter].background+"')";
}	
}
previous=function(){
try{
counter--;
backgroundImage.style.backgroundImage = "url('"+background[counter].background+"')";
}
catch(err){
counter=0;
backgroundImage.style.backgroundImage = "url('"+background[counter].background+"')";
}
}
slider = function(){
	sliderInterval =setInterval(setSliderImage, 3000);
}
setSliderImage = function(){
	try
	{
	counter++;
	backgroundImage.style.backgroundImage = "url('"+background[counter].background+"')";
	}
	catch(err)
	{
	counter = 0;
	backgroundImage.style.backgroundImage = "url('"+background[counter].background+"')";
	}
}
startSlider= function(){
	slider();
}
stopSlider= function(){
	clearInterval(sliderInterval);	
}

startslider.onclick= function(){
	startSlider();
}
stopslider.onclick=function(){
	stopSlider();
}
nextbutton.onclick=function(){
	next();
}
prev.onclick=function(){
	previous();
}
headerElement.onmouseover = function(){
	displayButtons();
}
headerElement.onmouseout = function(){
	hideButtons();
}

}